# Arch
1. This repository contains a post-installation script for my
[arch](https://wiki.archlinux.org/title/Arch_Linux) setup :)
1. This is just to bootstrap a new system, not to maintain it. That's your job :))
1. If you type anything wrong, consider your system **probably dead** and start over :)))
1. After a **successful** installation, the [Home Directory](https://gitlab.com/ekroll/homedir)
is a git repo that tracks **my** dotfiles. \
If you dont like that, then `sudo rm -r -d ~/.git` and **you** are free to *fork manually* :))))

### System applications
* Window manager -> [Window Manager](https://gitlab.com/ekroll/wm) (customized [dwm](https://dwm.suckless.org/))
* Terminal emulator -> [Terminal Emulator](https://gitlab.com/ekroll/te) (customized [st](https://st.suckless.org/))
* Text editor -> [vim](https://github.com/vim/vim)
* Web browser -> [brave](https://github.com/brave/brave-browser)
* Shell -> [bash](https://www.gnu.org/software/bash/)

It also installs [some](https://gitlab.com/ekroll/os-installer/-/blob/dev/install/Packages.list) 
other packages but it is a quite minimalistic system...


### No application launcher
Thanks to [window swallowing](https://dwm.suckless.org/patches/swallow/), I don't have an \
application launcher like 
[dmenu](https://tools.suckless.org/dmenu/) or 
[rofi](https://github.com/davatorium/rofi). \
I just use the terminal.


### Keybindings?
`Mod` + `W` -> **Open a window** *(always a terminal ofc)* \
`Mod` + `Shift` + `W` -> **Close focused window** *(or regain terminal)* \
`Mod` + `Tab` -> **Focus next window** \
`Mod` + `Shift` + `Tab` -> **Focus previous window** \
`Mod` + `[1-9]` -> **Change workspace** *(display windows tagged **x**)* \
`Mod` + `Shift` + `[1-9]` -> **Move window to workspace** *(Clear and tag focused window **x**)* \
`Mod` + `0` -> **View all workspaces** *(display windows tagged **\***)* \
`Mod` + `Shift` + `0` -> **Show window on all workspaces** *(tag focused window **\***)*

*Check/edit `~/.config/wm/config.def.h` the rest of them when setup has finished*


### Colorscheme
* Background -> `#000000`
* ~Middleground ...ish -> `#808080`
* Foreground -> `#eeeeee` 

*Vim, st, GTK and Qt has some more colors but sticks to the overall dark monochrome style*


### External resources
[Bash Hackers](https://wiki.bash-hackers.org/syntax/pe),
[Disconnected Systems](https://disconnected.systems/blog/archlinux-installer/#setting-variables-and-collecting-user-input)

# Not satisfied?
If you dont like this configuration, try out
[suicide linux](https://qntm.org/suicide)'s `sudo rm -rf /`
