# Update system and sync normal rice-packages
here=$(pwd)
archpkgs=$(cat ${here}/user/pkg.list)

#timeshift --create
sudo pacman -Syu
sudo pacman -S ${archpkgs}


# Sync user config
url=https://gitlab.com/ekroll/dotconfig.git

if [[ ! -d "${HOME/.git}" ]]
    sudo rm -rfd ${dest}
    git clone ${url} ${dest}
else
    cd; git pull
fi

gui=${HOME}/.config/gui
cd ${gui}/wm && make clean install
cd ${gui}/terminal && make clean install


# Sync AUR rice-packages
aurpath=${HOME}/.config/aur
aurpkgs=$(cat ${here}/user/aur.list)

for pkg in ${aurpkgs}
do
    url=https://aur.archlinux.org/${pkg}.git
    dest=${aurpath}/${pkg}

    if [[ -d "${dest}" ]];

    then
	cd ${dest}
	git pull

    else
	git clone ${url} ${dest}
	cd ${dest}

    makepkg -sirc
    fi
done


# Fix permissions and sync groups
shopt -s dotglob
chown -R ${USER}:${USER} ${HOME}
chown -R ${USER}:${USER} ${HOME}/*
usrgroups=$(cat ${here}/user/groups.list)

for group in ${usrgroups}
do
    usermod -a ${USER} -G ${group}
done

